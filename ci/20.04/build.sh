#!/bin/bash

set -Eeuo pipefail

TARGET_ARCH=${TARGET_ARCH:-armhf}
TAG=${TAG:-latest}
TARGET_TAG=${TARGET_TAG:-latest}
CLICKABLE_VERSION=${CLICKABLE_VERSION:-""}

# If the tag is a specific clickable version, we should ensure
# to install exactly that version in the image
if [[ $TAG =~ ^[0-9]+\.[0-9]+\.[0-9]+$ ]]; then
	CLICKABLE_VERSION="=${TAG}*"
fi

docker build -t clickable/ci-20.04-$TARGET_ARCH:$TAG \
    --build-arg TARGET_ARCH=$TARGET_ARCH \
    --build-arg TARGET_TAG=$TARGET_TAG \
    --build-arg CLICKABLE_VERSION=$CLICKABLE_VERSION \
    .

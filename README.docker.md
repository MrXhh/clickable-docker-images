# Clickable

Build and compile Ubuntu Touch apps easily from the command line. Deploy your
apps to your Ubuntu Touch device for testing or test them on any desktop Linux
distribution. Get logs for debugging and directly access a terminal on your device.

These images are used by Clickable to provide build environments for Ubuntu Touch
app developers. You do not need to download these separately, Clickable will
handle that for you.

## Source

The source for all Dockerfiles used to create the Clickable images can be found
on [GitLab](https://gitlab.com/clickable/clickable-docker-images/).

## Tags

Clickable will automatically use the `latest` tag when pulling images. However,
you may want to stick to a specific version when running in a CI environment.
To that end, we tag images with the Clickable version (ex: `7.11.0`) and the
major version (ex: `7`).

## License

Copyright (C) 2023 Clickable Team

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!/bin/bash

set -xe

for tag in "$@"; do
  docker tag $1 $tag
  docker push $tag
done
